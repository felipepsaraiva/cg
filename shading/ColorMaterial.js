const { vec4 } = glMatrix;

export default class ColorMaterial {
  constructor() {
    this.emission = vec4.fromValues(0, 0, 0, 0);
    this.ambient = vec4.fromValues(1, 1, 1, 1);
    this.diffuse = vec4.fromValues(1, 1, 1, 1);
    this.specular = vec4.fromValues(1, 1, 1, 1);
    this.shininess = 100.0;
  }

  setEmission(color) {
    this.emission = vec4.clone(color);
  }

  setColor(color, includeEmission = false) {
    this.ambient = vec4.clone(color);
    this.diffuse = vec4.clone(color);
    if (includeEmission) this.setEmissionColor(color);
  }

  setShininess(shininess) {
    this.shininess = shininess;
    if (shininess) this.specular = vec4.fromValues(1, 1, 1, 1);
    else this.specular = vec4.fromValues(0, 0, 0, 1);
  }

  apply(p) {
    const gl = p.gl;
    gl.uniform4fv(p.uniform('material.emission'), this.emission);
    gl.uniform4fv(p.uniform('material.ambient'), this.ambient);
    gl.uniform4fv(p.uniform('material.diffuse'), this.diffuse);
    gl.uniform4fv(p.uniform('material.specular'), this.specular);
    gl.uniform1f(p.uniform('material.shininess'), this.shininess);
  }
}
