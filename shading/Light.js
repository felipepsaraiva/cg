const { vec3, vec4 } = glMatrix;

export default class Light {
  constructor() {
    this.ambient = vec4.fromValues(0.15, 0.15, 0.15, 1.0);
    this.diffuse = vec4.fromValues(1.0, 1.0, 1.0, 1.0);
    this.specular = vec4.fromValues(1.0, 1.0, 1.0, 1.0);

    this.decay = {
      constant: 1.0,
      linear: 0.0,
      quadratic: 0.05
    };

    this.position = vec3.fromValues(0, 0, 0);
  }

  apply(p) {
    const gl = p.gl;
    gl.uniform3fv(p.uniform('lightPosition'), this.position);

    gl.uniform4fv(p.uniform('light.ambient'), this.ambient);
    gl.uniform4fv(p.uniform('light.diffuse'), this.diffuse);
    gl.uniform4fv(p.uniform('light.specular'), this.specular);

    gl.uniform1f(p.uniform('lightDecay.constant'), this.decay.constant);
    gl.uniform1f(p.uniform('lightDecay.linear'), this.decay.linear);
    gl.uniform1f(p.uniform('lightDecay.quadratic'), this.decay.quadratic);
  }
}
