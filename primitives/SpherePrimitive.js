// Based on the algorithm from: https://www.songho.ca/opengl/gl_sphere.html

const { vec3 } = glMatrix;

const SpherePrimitive = {
  id: 'SpherePrimitive',
  sectors: 36,
  stacks: 18,
  radius: 0.5,

  generateVertices: function(generateNormals = true) {
    const sectorStep = 2 * Math.PI / this.sectors;
    const stackStep = Math.PI / this.stacks;

    const vertices = [], normals = [];
    let vertex, normal;
    let sectorAngle, stackAngle;
    let x, y, z, xz;

    for (let i = 0; i <= this.stacks; i++) {
      stackAngle = (Math.PI / 2) - (i * stackStep);
      xz = this.radius * Math.cos(stackAngle);
      y = this.radius * Math.sin(stackAngle);

      for (let j = 0; j <= this.sectors; j++) {
        sectorAngle = j * sectorStep;
        x = xz * Math.sin(sectorAngle);
        z = xz * Math.cos(sectorAngle);

        vertex = vec3.fromValues(x, y, z);
        vertices.push(vertex);

        if (generateNormals) {
          normal = vec3.create();
          vec3.normalize(normal, vertex);
          normals.push(normal);
        }
      }
    }

    return { vertices, normals };
  },

  generateIndexSegments() {
    const indices = [];
    let k1, k2;

    for (let i = 0; i < this.stacks; i++) {
      k1 = i * (this.sectors + 1);
      k2 = k1 + this.sectors + 1;

      for (let j = 0; j < this.sectors; j++ , k1++ , k2++) {
        // k1 => k2 => k1+1
        indices.push(k1);
        indices.push(k2);
        indices.push(k1 + 1);

        // k1+1 => k2 => k2+1
        indices.push(k1 + 1);
        indices.push(k2);
        indices.push(k2 + 1);
      }
    }

    return [indices];
  },

  get directives() {
    const { vertices, normals } = this.generateVertices();
    return {
      vertices,
      normals,
      indexSegments: this.generateIndexSegments()
    };
  }
};

export default SpherePrimitive;
