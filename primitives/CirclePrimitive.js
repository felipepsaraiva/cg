const { vec3 } = glMatrix;

const CirclePrimitive = {
  name: 'CirclePrimitive',
  sectors: 36,
  radius: 0.5,

  get vertices() {
    const sectorStep = 2 * Math.PI / this.sectors;

    const vertices = [vec3.fromValues(0, 0, 0)];
    let sectorAngle, x, y;

    for (let i = 0; i <= this.sectors; i++) {
      sectorAngle = i * sectorStep;
      x = this.radius * Math.cos(sectorAngle);
      y = this.radius * Math.sin(sectorAngle);

      vertices.push(vec3.fromValues(x, y, 0));
    }

    return vertices;
  },

  get indices() {
    const indices = [0];
    for (let i = 0; i <= this.sectors; i++)
      indices.push(i + 1);

    return [indices];
  },

  get colors() {
    return this.vertices.map((val, i) => i % 2 ? [0.7, 0.7, 0.7, 1] : [0.2, 0.2, 0.2, 1]);
  },

  get directives() {
    return {
      vertices: this.vertices,
      colors: this.colors,
      indexSegments: this.indices
    };
  }
};

export default CirclePrimitive;
