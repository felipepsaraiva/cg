const { vec3 } = glMatrix;

const CubePrimitive = {
  id: 'CubePrimitive',

  vertices: [
    vec3.fromValues(-0.5, -0.5, 0.5),
    vec3.fromValues(-0.5, 0.5, 0.5),
    vec3.fromValues(0.5, 0.5, 0.5),
    vec3.fromValues(0.5, -0.5, 0.5),
    vec3.fromValues(-0.5, -0.5, -0.5),
    vec3.fromValues(-0.5, 0.5, -0.5),
    vec3.fromValues(0.5, 0.5, -0.5),
    vec3.fromValues(0.5, -0.5, -0.5)
  ],

  indices: [
    [
      1, 0, 3,
      3, 2, 1,
      2, 3, 7,
      7, 6, 2,
      3, 0, 4,
      4, 7, 3,
      6, 5, 1,
      1, 2, 6,
      4, 5, 6,
      6, 7, 4,
      5, 4, 0,
      0, 1, 5
    ]
  ],

  colors: [
    [0.2, 0.2, 0.2, 1],
    [1, 0, 0, 1],
    [1, 1, 0, 1],
    [0, 1, 0, 1],
    [0, 0, 1, 1],
    [1, 0, 1, 1],
    [0.8, 0.8, 0.8, 1],
    [0, 1, 1, 1],
  ],

  get directives() {
    return {
      vertices: this.vertices,
      colors: this.colors,
      indexSegments: this.indices
    };
  }
};

export default CubePrimitive;
