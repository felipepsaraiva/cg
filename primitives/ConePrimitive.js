import CirclePrimitive from '/primitives/CirclePrimitive.js';

const { glMatrix: glm, vec3 } = glMatrix;

const ConePrimitive = {
  id: 'ConePrimitive',

  get vertices() {
    const circle_vertices = CirclePrimitive.vertices.map((vertex) => {
      vec3.rotateX(vertex, vertex, [0, 0, 0], glm.toRadian(90));
      return vertex;
    });
    return [vec3.fromValues(0, 1, 0)].concat(circle_vertices);
  },

  get indices() {
    const bottom = CirclePrimitive.indices[0].map((val) => val + 1);
    const lateral = bottom.slice();
    lateral[0] = 0;

    return [bottom, lateral];
  },

  get colors() {
    return this.vertices.map((val, i) => i % 2 ? [0.7, 0.7, 0.7, 1] : [0.2, 0.2, 0.2, 1]);
  },

  get directives() {
    return {
      vertices: this.vertices,
      colors: this.colors,
      indexSegments: this.indices
    };
  }
}

export default ConePrimitive;
