export function getCanvas(id = 'canvas') {
  const canvas = document.getElementById(id);
  if (!canvas) throw 'Canvas not found!';

  resizeCanvas(canvas);
  return canvas;
}

export function getContext(canvas = 'canvas') {
  if (typeof canvas == 'string')
    canvas = getCanvas(canvas);

  const gl = canvas.getContext('webgl');
  if (!gl) throw 'Could not get context!';

  return gl;
}

export function getAspect(canvas) {
  if (typeof canvas == 'string')
    canvas = getCanvas(canvas);

  return canvas.width / canvas.height;
}

export function resizeCanvas(canvas) {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
}
