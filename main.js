'use strict';

import { getCanvas, getContext } from '/utils.js';
import Program from '/webgl/Program.js';
import Shader from '/webgl/Shader.js';
import PrimitivesBuffer from '/webgl/PrimitivesBuffer.js';

import SolarSystemScene from '/scenes/SolarSystemScene.js';

window.main = async function main() {
  const canvas = getCanvas();
  const gl = getContext(canvas);
  gl.enable(gl.DEPTH_TEST);

  const vertex = await Shader.loadAndCreate(gl, 'vshader.glsl', gl.VERTEX_SHADER);
  const fragment = await Shader.loadAndCreate(gl, 'fshader.glsl', gl.FRAGMENT_SHADER);

  const program = new Program(canvas, gl, vertex, fragment);
  program.clearColor = [1, 1, 1, 1];

  const primitivesBuffer = new PrimitivesBuffer(program);
  const solarSystem = new SolarSystemScene(program, primitivesBuffer);
  program.setScene(solarSystem);

  program.startRendering();
};
