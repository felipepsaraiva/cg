'use strict';

export default class GLBuffer {
  constructor(gl, bufferType = gl.ARRAY_BUFFER, dataType = Float32Array) {
    this.gl = gl;
    this.type = bufferType;
    this.dataType = dataType;
    this.ref = gl.createBuffer();

    this.data = [];
    this.flattenedData = [];
  }

  setData(data) {
    this.data = data;
    this.flattenedData = GLBuffer.flatten(data);
  }

  appendData(data) {
    const offset = this.flattenedData.length * this.dataType.BYTES_PER_ELEMENT;
    this.data = this.data.concat(data);
    this.flattenedData = this.flattenedData.concat(GLBuffer.flatten(data));
    return offset;
  }

  bufferData(usage = this.gl.STATIC_DRAW) {
    this.bind();
    this.gl.bufferData(this.type, this.dataType.from(this.flattenedData), usage);
  }

  bind() {
    this.gl.bindBuffer(this.type, this.ref);
  }

  get size() {
    return this.data.length;
  }

  static flatten(array) {
    return array.reduce((acc, val) => Array.isArray(val) || ArrayBuffer.isView(val) ? acc.concat(GLBuffer.flatten(val)) : acc.concat(val), []);
  }
}
