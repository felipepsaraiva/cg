'use strict';

export default class Shader {
  static async loadAndCreate(gl, name, type) {
    const headers = new Headers();
    headers.append('pragma', 'no-cache');
    headers.append('cache-control', 'no-cache');

    const source = await fetch(`/shaders/${name}`, { headers }).then(res => res.text());
    return new Shader(gl, name, type, source);
  }

  constructor(gl, name, type, source) {
    this.gl = gl;
    this.name = name;

    this.ref = this.gl.createShader(type);
    this.gl.shaderSource(this.ref, source);
    this.gl.compileShader(this.ref);

    this.checkErrors();
  }

  checkErrors() {
    if (!this.gl.getShaderParameter(this.ref, this.gl.COMPILE_STATUS)) {
      console.error(`Could not compile WebGL shader "${this.name}"!`);
      throw this.gl.getShaderInfoLog(this.ref);
    }
  }
}
