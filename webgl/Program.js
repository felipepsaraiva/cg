'use strict';

export default class Program {
  constructor(canvas, gl, vertex, fragment) {
    this.canvas = canvas;
    this.gl = gl;
    this.vertex = vertex;
    this.fragment = fragment;

    this.ref = this.gl.createProgram();
    this.gl.attachShader(this.ref, this.vertex.ref);
    this.gl.attachShader(this.ref, this.fragment.ref);
    this.gl.linkProgram(this.ref);

    this.checkErrors();

    this.attributes = {};
    this.uniforms = {};

    this.scene = null;

    this.currentRender = 0;
    this.lastRender = 0;
    this.delta = 0;

    this.render = this.render.bind(this);
  }

  checkErrors() {
    if (!this.gl.getProgramParameter(this.ref, this.gl.LINK_STATUS)) {
      console.error('Could not compile WebGL program!');
      throw this.gl.getProgramInfoLog(this.ref);
    }
  }

  use() {
    this.gl.useProgram(this.ref);
  }

  setupAttr(name) {
    this.attributes[name] = this.gl.getAttribLocation(this.ref, name);
  }

  attr(name) {
    if (!this.attributes[name]) this.setupAttr(name);
    return this.attributes[name];
  }

  enableAttr(name, size = 4, type = this.gl.FLOAT, normalized = false, stride = 0, offset = 0) {
    const attr = this.attr(name);
    this.gl.vertexAttribPointer(attr, size, type, normalized, stride, offset);
    this.gl.enableVertexAttribArray(attr);
  }

  setupUniform(name) {
    this.uniforms[name] = this.gl.getUniformLocation(this.ref, name);
  }

  uniform(name) {
    if (!this.uniforms[name]) this.setupUniform(name);
    return this.uniforms[name];
  }

  setScene(scene) {
    this.scene = scene;
  }

  startRendering() {
    this.use();
    requestAnimationFrame(this.render);
  }

  render(ms) {
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.COLOR_DEPTH_BIT);

    this.currentRender = ms / 1000;
    this.delta = this.currentRender - this.lastRender;

    if (this.scene) this.scene.render();

    this.lastRender = this.currentRender;
    requestAnimationFrame(this.render);
  }

  set clearColor(color) {
    this.gl.clearColor(...color);
  }

  get time() {
    return this.currentRender;
  }
}
