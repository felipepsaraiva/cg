import GLBuffer from '/webgl/GLBuffer.js';
import CubePrimitive from '/primitives/CubePrimitive.js';
import SpherePrimitive from '/primitives/SpherePrimitive.js';
import ConePrimitive from '/primitives/ConePrimitive.js';

export default class PrimitivesBuffer {
  constructor(program, bufferData = true) {
    this.program = program;
    const gl = program.gl;

    this.vertexBuffer = new GLBuffer(gl, gl.ARRAY_BUFFER, Float32Array);
    this.normalBuffer = new GLBuffer(gl, gl.ARRAY_BUFFER, Float32Array);
    this.indexBuffer = new GLBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, Uint16Array);

    this.dataSegments = {};

    this.addPrimitive(SpherePrimitive);
    // this.addPrimitive(ConePrimitive);
    // this.addPrimitive(CubePrimitive);

    if (bufferData) this.bufferData();
  }

  addPrimitive(primitive) {
    const { vertices, normals, indexSegments } = primitive.directives;
    const vertexOffset = this.vertexBuffer.data.length;

    this.vertexBuffer.appendData(vertices);
    this.normalBuffer.appendData(normals);

    this.dataSegments[primitive.id] = indexSegments.map((segment) => {
      segment = segment.map((value) => value + vertexOffset);
      return {
        size: segment.length,
        offset: this.indexBuffer.appendData(segment)
      };
    });
  }

  bufferData() {
    this.vertexBuffer.bufferData();
    this.program.enableAttr('position', 3);

    this.normalBuffer.bufferData();
    this.program.enableAttr('normal', 3);

    this.indexBuffer.bufferData();
  }

  getDataSegments(primitive) {
    return this.dataSegments[primitive.id];
  }
}
