const { mat4 } = glMatrix;

export default class GraphStack {
  constructor(bottom) {
    this.reset(bottom);
  }

  reset(bottom = mat4.create()) {
    this.stack = [bottom];
  }

  top() {
    return this.stack[this.stack.length - 1];
  }

  push() {
    this.stack.push(mat4.clone(this.top()));
    return this.top();
  }

  pop() {
    if (this.stack.length > 1) this.stack.pop();
    return this.top();
  }
}
