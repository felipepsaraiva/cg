import { getAspect } from '/utils.js';
import Scene from '/scenes/Scene.js';
import GraphStack from '/scenes/GraphStack.js';
import PerspectiveCamera from '/camera/PerspectiveCamera.js';
import Sphere from '/models/Sphere.js';

const { glMatrix: glm, mat4, vec3 } = glMatrix;

export default class SolarSystemScene extends Scene {
  constructor(program, primitivesBuffer) {
    super(program, primitivesBuffer);
    this.camera = new PerspectiveCamera(60, getAspect(program.canvas));
    this.stack = new GraphStack();
    this.m = this.stack.top();

    this.sun = new Sphere(primitivesBuffer, false);
    this.sun.shouldDrawLines = true;
    this.sun.material.setEmission([1, 0.65, 0, 1]);

    this.earth = new Sphere(primitivesBuffer, false);
    this.earth.shouldDrawLines = true;
    this.earth.material.setColor([0.25, 0.5, 1, 1]);
    // this.earth.material.setShininess(0);

    this.moon = new Sphere(primitivesBuffer, false);
    this.moon.shouldDrawLines = true;
    this.moon.material.setColor([0.65, 0.65, 0.65, 1]);
    // this.moon.material.setShininess(0);

    this.init();
  }

  init() {
    this.camera.eye = [0, 2, 5];
    this.camera.up = [0, 1, 0];
    this.camera.setCenter([0, 0, 0]);

    this.state = {
      moveCameraUV: false,
      sun: {
        size: 2,
        rot: 0,
        rotSpeed: 15,
      },
      earth: {
        gRot: 0,
        gRotSpeed: 40,
        gTrans: 3,
        eRot: 0,
        eRotSpeed: 120,
        size: 1,
      },
      moon: {
        rot: 0,
        rotSpeed: 90,
        trans: 1.2,
        size: 0.4,
      }
    };
  }

  render() {
    this.updateState();
    super.render();
    this.stack.reset();

    this.m = this.stack.push();
    mat4.rotateY(this.m, this.m, glm.toRadian(this.state.sun.rot));
    mat4.scale(this.m, this.m, [this.state.sun.size, this.state.sun.size, this.state.sun.size]);
    this.sun.render(this.program, this.m);
    this.m = this.stack.pop();

    mat4.rotateY(this.m, this.m, glm.toRadian(this.state.earth.gRot));
    mat4.translate(this.m, this.m, [this.state.earth.gTrans, 0, 0]);

    this.m = this.stack.push();
    mat4.rotateY(this.m, this.m, glm.toRadian(this.state.earth.eRot));
    mat4.scale(this.m, this.m, [this.state.earth.size, this.state.earth.size, this.state.earth.size]);
    this.earth.render(this.program, this.m);
    this.m = this.stack.pop();

    mat4.rotateY(this.m, this.m, glm.toRadian(this.state.moon.rot));
    mat4.translate(this.m, this.m, [this.state.moon.trans, 0, 0]);
    mat4.rotateY(this.m, this.m, glm.toRadian(-this.state.moon.rot));
    mat4.scale(this.m, this.m, [this.state.moon.size, this.state.moon.size, this.state.moon.size]);
    this.moon.render(this.program, this.m);
  }

  updateState() {
    const { delta } = this.program;
    this.state.sun.rot += this.state.sun.rotSpeed * delta;
    this.state.earth.gRot += this.state.earth.gRotSpeed * delta;
    this.state.earth.eRot += this.state.earth.eRotSpeed * delta;
    this.state.moon.rot += this.state.moon.rotSpeed * delta;
  }

  registerListeners() {
    super.registerListeners();
    document.addEventListener('keyup', this.keyUpListener.bind(this));
    document.addEventListener('mousemove', this.mouseMoveListener.bind(this));
    this.program.canvas.addEventListener('click', this.mouseClickListener.bind(this));
  }

  keyDownListener(e) {
    switch (e.key.toLowerCase()) {
      case 'w':
        this.camera.velocity[2] = 0.75;
        break;
      case 's':
        this.camera.velocity[2] = -0.75;
        break;

      case 'a':
        this.camera.velocity[0] = 1.5;
        break;
      case 'd':
        this.camera.velocity[0] = -1.5;
        break;

      case ' ':
        this.camera.velocity[1] = 1.5;
        break;
      case 'shift':
        this.camera.velocity[1] = -1.5;
        break;

      case 'q':
        this.camera.angularVelocity[2] = 60;
        break;
      case 'e':
        this.camera.angularVelocity[2] = -60;
        break;
    }

    return super.keyDownListener(e);
  }

  keyUpListener(e) {
    switch (e.key.toLowerCase()) {
      case 'w':
        this.camera.velocity[2] = 0;
        break;
      case 's':
        this.camera.velocity[2] = 0;
        break;

      case 'a':
        this.camera.velocity[0] = 0;
        break;
      case 'd':
        this.camera.velocity[0] = 0;
        break;

      case ' ':
        this.camera.velocity[1] = 0;
        break;
      case 'shift':
        this.camera.velocity[1] = 0;
        break;

      case 'q':
        this.camera.angularVelocity[2] = 0;
        break;
      case 'e':
        this.camera.angularVelocity[2] = 0;
        break;
    }
  }

  mouseClickListener(e) {
    this.program.canvas.requestPointerLock();
  }

  mouseMoveListener(e) {
    if (document.pointerLockElement != this.program.canvas) return;

    const rotation = mat4.create();
    mat4.fromRotation(rotation, glm.toRadian(e.movementY / 10), this.camera.u);
    vec3.transformMat4(this.camera.direction, this.camera.direction, rotation);
    vec3.transformMat4(this.camera.up, this.camera.up, rotation);

    mat4.fromRotation(rotation, glm.toRadian(-e.movementX / 10), [0, 1, 0]);
    vec3.transformMat4(this.camera.direction, this.camera.direction, rotation);
    vec3.transformMat4(this.camera.up, this.camera.up, rotation);
  }
}
