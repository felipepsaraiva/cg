import { getAspect, resizeCanvas } from '/utils.js';
import Camera from '/camera/Camera.js';
import Light from '/shading/Light.js';

export default class Scene {
  constructor(program, primitivesBuffer, registerListeners = true) {
    this.program = program;
    this.camera = new Camera();
    this.light = new Light();
    if (registerListeners) this.registerListeners();
  }

  init() {}

  render() {
    const { gl, time, delta } = this.program;
    this.light.apply(this.program);

    this.camera.update(time, delta);
    gl.uniformMatrix4fv(this.program.uniform('projection'), false, this.camera.projection());
    gl.uniformMatrix4fv(this.program.uniform('view'), false, this.camera.lookAt());
  }

  registerListeners() {
    window.addEventListener('resize', this.windowResizeListener.bind(this));
    document.addEventListener('keydown', this.keyDownListener.bind(this));
  }

  windowResizeListener() {
    const { canvas, gl } = this.program;
    resizeCanvas(canvas);
    gl.viewport(0, 0, canvas.width, canvas.height);
    this.camera.aspect = getAspect(canvas);
  }

  keyDownListener(e) {
    if (e.key == 'Enter') this.init();
  }

  get p() {
    return this.program;
  }
}
