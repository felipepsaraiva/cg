import Camera from '/camera/Camera.js';

const { glMatrix: glm, mat4 } = glMatrix;

export default class OrthogonalCamera extends Camera {
  constructor(height = 1, aspect = 1, depth = 100) {
    super();

    const halfh = height/2;
    const halfw = halfh * aspect;

    // X plane
    this.left = -halfw;
    this.right = halfw;
    // Y plane
    this.bottom = -halfh;
    this.top = halfh;
    // Z plane
    this.near = 0;
    this.far = depth;
  }

  setPlanes(left = -1, right = 1, bottom = -1, top = 1, near = 0, far = 100) {
    this.left = left;
    this.right = right;
    this.bottom = bottom;
    this.top = top;
    this.near = near;
    this.far = far;
  }

  projection() {
    mat4.ortho(this.proj, this.left, this.right, this.bottom, this.top, this.near, this.far);
    return this.proj;
  }
}
