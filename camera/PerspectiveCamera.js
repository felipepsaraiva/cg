import Camera from '/camera/Camera.js';

const { glMatrix: glm, mat4 } = glMatrix;

export default class PerspectiveCamera extends Camera {
  constructor(fovy = 90, aspect = 1, near = 0.1, far = 1000) {
    super();

    this.fovy = fovy;
    this.aspect = aspect;
    this.near = near;
    this.far = far;
  }

  projection() {
    mat4.perspective(this.proj, glm.toRadian(this.fovy), this.aspect, this.near, this.far);
    return this.proj;
  }
}
