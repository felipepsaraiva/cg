const { glMatrix: glm, mat4, vec3 } = glMatrix;

export default class Camera {
  constructor() {
    this.eye = vec3.fromValues(0, 0, 1);
    this.center = vec3.fromValues(0, 0, 0);
    this.direction = vec3.fromValues(0, 0, -1);
    this.up = vec3.fromValues(0, 1, 0);

    this.u = vec3.create();
    this.v = vec3.create();

    this.velocity = [0, 0, 0];
    this.angularVelocity = [0, 0, 0];

    this.transform = mat4.create();
    this.view = mat4.create();
    this.proj = mat4.create();
  }

  setCenter(center) {
    this.center = center;
    vec3.sub(this.direction, this.center, this.eye);
  }

  calculateUV() {
    vec3.cross(this.u, this.up, this.direction);
    vec3.normalize(this.u, this.u);

    vec3.cross(this.v, this.direction, this.u);
    vec3.normalize(this.v, this.v);
  }

  lookAt() {
    vec3.add(this.center, this.eye, this.direction);
    mat4.lookAt(this.view, this.eye, this.center, this.up);
    return this.view;
  }

  projection() {
    return this.proj;
  }

  update(time, delta) {
    this.calculateUV();

    if (this.velocity[0])
      vec3.scaleAndAdd(this.eye, this.eye, this.u, this.velocity[0] * delta);
    if (this.velocity[1])
      vec3.scaleAndAdd(this.eye, this.eye, this.v, this.velocity[1] * delta);
    if (this.velocity[2])
      vec3.scaleAndAdd(this.eye, this.eye, this.direction, this.velocity[2] * delta);

    if (this.angularVelocity[0]) {
      mat4.fromRotation(this.transform, glm.toRadian(this.angularVelocity[0] * delta), this.u);
      vec3.transformMat4(this.direction, this.direction, this.transform);
      vec3.transformMat4(this.up, this.up, this.transform);
    }

    if (this.angularVelocity[1]) {
      mat4.fromRotation(this.transform, glm.toRadian(this.angularVelocity[1] * delta), this.v);
      vec3.transformMat4(this.direction, this.direction, this.transform);
      vec3.transformMat4(this.up, this.up, this.transform);
    }

    if (this.angularVelocity[2]) {
      mat4.fromRotation(this.transform, glm.toRadian(this.angularVelocity[2] * delta), this.direction);
      vec3.transformMat4(this.up, this.up, this.transform);
    }
  }
}
