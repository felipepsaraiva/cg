precision mediump float;

struct Material {
  vec4 emission;
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  float shininess;
};

struct Light {
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
};

uniform Material material;
uniform Light light;

varying vec3 N, L, E;
varying float decay;

void main() {
  vec3 n = normalize(N);
  vec3 l = normalize(L);
  vec3 e = normalize(E);
  vec3 h = normalize(l + e);

  vec4 ambientProduct = material.ambient * light.ambient;
  vec4 diffuseProduct = material.diffuse * (light.diffuse * decay);
  vec4 specularProduct = material.specular * (light.specular * decay);

  vec4 ambient = ambientProduct;
  vec4 diffuse = max(dot(l, n), 0.0) * diffuseProduct;
  vec4 specular = pow(max(dot(n, h), 0.0), material.shininess) * specularProduct;

  if (dot(l, n) < 0.0)
    specular = vec4(0.0, 0.0, 0.0, 1.0);

  vec4 color;
  if (material.emission.a == 1.0) color = material.emission;
  else color = ambient + diffuse + specular;
  color.a = 1.0;

  gl_FragColor = color;
}
