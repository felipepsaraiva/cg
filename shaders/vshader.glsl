struct LightDecay {
  float constant;
  float linear;
  float quadratic;
};

attribute vec3 position;
attribute vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPosition;
uniform LightDecay lightDecay;

varying vec3 N, L, E;
varying float decay;

void main() {
  vec4 viewLightPosition = view * vec4(lightPosition, 1.0);
  vec4 viewPosition = view * model * vec4(position, 1.0);
  vec4 viewNormal = view * model * vec4(normal, 0.0);

  float lightPointDistance = distance(viewLightPosition, viewPosition);
  decay = 1.0 / (lightDecay.constant + lightDecay.linear * lightPointDistance + lightDecay.quadratic * pow(lightPointDistance, 2.0));

  N = viewNormal.xyz;
  L = (viewLightPosition - viewPosition).xyz;
  E = -viewPosition.xyz;

  gl_Position = projection * viewPosition;
}
