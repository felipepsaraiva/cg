const { glMatrix: glm, mat4 } = glMatrix;

export default class Body {
  constructor() {
    this.position = [0, 0, 0];
    this.rotation = [0, 0, 0];
    this.scale = [1, 1, 1];

    this.velocity = [0, 0, 0];
    this.angularVelocity = [0, 0, 0];
  }

  update(time, delta) {
    this.position[0] += this.velocity[0] * delta;
    this.position[1] += this.velocity[1] * delta;
    this.position[2] += this.velocity[2] * delta;

    this.rotation[0] += this.angularVelocity[0] * delta;
    this.rotation[1] += this.angularVelocity[1] * delta;
    this.rotation[2] += this.angularVelocity[2] * delta;
  }

  transform(model) {
    mat4.translate(model, model, this.position);
    mat4.rotateZ(model, model, glm.toRadian(this.rotation[2]));
    mat4.rotateY(model, model, glm.toRadian(this.rotation[1]));
    mat4.rotateX(model, model, glm.toRadian(this.rotation[0]));
    mat4.scale(model, model, this.scale);
    return model;
  }
}
