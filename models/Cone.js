import Model from '/models/Model.js';
import ConePrimitive from '/primitives/ConePrimitive.js';

const { mat4 } = glMatrix;

export default class Cone extends Model {
  constructor(buffer, createBody) {
    super(createBody);

    this.dataSegments = buffer.getDataSegments(ConePrimitive);
    this.bottomSegment = this.dataSegments[0];
    this.lateralSegment = this.dataSegments[1];
  }

  render(p, modelTransform = mat4.create()) {
    super.render(p, modelTransform);
    const gl = p.gl;

    gl.uniform4fv(p.uniform('color'), this.material);
    gl.uniformMatrix4fv(p.uniform('model'), false, modelTransform);
    gl.drawElements(gl.TRIANGLE_FAN, this.bottomSegment.size, gl.UNSIGNED_SHORT, this.bottomSegment.offset);
    gl.drawElements(gl.TRIANGLE_FAN, this.lateralSegment.size, gl.UNSIGNED_SHORT, this.lateralSegment.offset);

    if (this.shouldDrawLines) this.drawLines(p);
  }
}
