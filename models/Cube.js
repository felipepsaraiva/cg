import Model from '/models/Model.js';
import CubePrimitive from '/primitives/CubePrimitive.js';

const { mat4 } = glMatrix;

export default class Cube extends Model {
  constructor(buffer, createBody) {
    super(createBody);

    this.dataSegments = buffer.getDataSegments(CubePrimitive);
    this.dataSize = this.dataSegments[0].size;
    this.dataOffset = this.dataSegments[0].offset;
  }

  render(p, modelTransform = mat4.create()) {
    super.render(p, modelTransform);
    const gl = p.gl;

    gl.uniform4fv(p.uniform('color'), this.material);
    gl.uniformMatrix4fv(p.uniform('model'), false, modelTransform);
    gl.drawElements(gl.TRIANGLES, this.dataSize, gl.UNSIGNED_SHORT, this.dataOffset);

    if (this.shouldDrawLines) this.drawLines(p);
  }
}
