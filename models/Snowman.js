import Model from '/models/Model.js';
import Sphere from '/models/Sphere.js';
import Cone from '/models/Cone.js';

import GraphStack from '/scenes/GraphStack.js';

const { glMatrix: glm, mat4 } = glMatrix;

export default class Snowman extends Model {
  constructor(buffer, createBody) {
    super(createBody);
    this.material = [0.85, 0.85, 0.85, 1];

    this.stack = new GraphStack();
    this.m = this.stack.top();

    this.bottom = new Sphere(buffer, false);
    this.bottom.material = this.material;
    this.bottom.shouldDrawLines = true;

    this.head = new Sphere(buffer, false);
    this.head.material = this.material;
    this.head.shouldDrawLines = true;

    this.nose = new Cone(buffer, false);
    this.nose.material = this.material;
    this.nose.shouldDrawLines = true;
  }

  render(p, modelTransform = mat4.create()) {
    super.render(p, modelTransform);
    this.stack.reset(modelTransform);

    mat4.translate(this.m, this.m, [0, 0.325, 0]);

    this.m = this.stack.push();
    mat4.scale(this.m, this.m, [0.65, 0.65, 0.65]);
    this.bottom.render(p, this.m);
    this.m = this.stack.pop();

    mat4.translate(this.m, this.m, [0, 0.45, 0]);

    this.m = this.stack.push();
    mat4.scale(this.m, this.m, [0.45, 0.45, 0.45]);
    this.head.render(p, this.m);
    this.m = this.stack.pop();

    mat4.translate(this.m, this.m, [0, 0, 0.15]);
    mat4.rotateX(this.m, this.m, glm.toRadian(90));
    mat4.scale(this.m, this.m, [0.11, 0.25, 0.11]);
    this.nose.render(p, this.m);
  }
}
