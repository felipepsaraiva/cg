import Model from '/models/Model.js';
import SpherePrimitive  from '/primitives/SpherePrimitive.js';

const { mat4 } = glMatrix;

export default class Sphere extends Model {
  constructor(buffer, createBody) {
    super(createBody);

    this.dataSegments = buffer.getDataSegments(SpherePrimitive);
    this.dataSize = this.dataSegments[0].size;
    this.dataOffset = this.dataSegments[0].offset;
  }

  render(p, modelTransform = mat4.create()) {
    super.render(p, modelTransform);
    const gl = p.gl;

    gl.uniformMatrix4fv(p.uniform('model'), false, modelTransform);
    gl.drawElements(gl.TRIANGLES, this.dataSize, gl.UNSIGNED_SHORT, this.dataOffset);

    if (this.shouldDrawLines) this.drawLines(p);
  }
}
