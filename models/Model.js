import Body from '/models/Body.js';
import ColorMaterial from '/shading/ColorMaterial.js';

const { mat4 } = glMatrix;

export default class Model {
  constructor(createBody = true) {
    this.lineColor = [0, 0, 0, 1];
    this.material = new ColorMaterial();

    this.shouldDrawLines = false;
    if (createBody) this.body = new Body();
  }

  hasBody() {
    return this.body;
  }

  render(p, modelTransform = mat4.create()) {
    this.material.apply(p);

    if (this.hasBody()) {
      this.body.update(p.time, p.delta);
      this.body.transform(modelTransform);
    }
  }

  drawLines(p) {
    if (!this.dataSegments) return;

    const gl = p.gl;
    gl.uniform4fv(p.uniform('color'), this.lineColor);

    this.dataSegments.forEach(({ size, offset }) => {
      gl.drawElements(gl.LINE_STRIP, size, gl.UNSIGNED_SHORT, offset);
    });
  }
}
